﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Resources;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ÁrbolesBiinarios
{
    class Nodo      //Nodo de un árbol o inicio de un árbol
    {
        string contenido;       //Contenido del nodo
        Nodo izq, der;      //Referencias al hijo izquierdo y el hijo derecho del árbol
        public Nodo(string dato = null)       //Contructor de nodos tanto vacíos como con datos
        {
            contenido = dato;
            this.izq = this.der = null;
        }
        public bool Vacio()     //Devuelve true si el árbol está vacío
        {
            bool respuesta = false;
            if (izq == null && der == null && contenido == null)
                respuesta = true;
            return respuesta;
        }
        public void DatosPrueba()        //Genera un árbol con los datos que se han estado trabajando
        {
            Nodo p;
            contenido = "A";        //Se pone como contenido del nodo this a A
            izq = null;     //Se elimina lo que haya a la izquierda del nodo A
            der = null;     //Se eliminaz lo que haya a la derecha del nodo A
            izq = new Nodo("B");
            p = izq;        //p queda apuntando al nodo B
            p.izq = new Nodo("C");
            p.der = new Nodo("D");
            der = new Nodo("B");
            p = der;        //p queda apuntando al nodo E
            p.izq = new Nodo ("T");       //
            p.der = new Nodo("F");
            p = p.der;      //p avanza por su derecha de un nodo, llega a F
            p.izq = new Nodo("G");
            p.der = new Nodo("H");
        }
        public void Recorrido(int tipo)
        {
            if (!Vacio())
            {
                switch (tipo)
                {
                    case 1:     //Recorrido en preorden
                        Console.Write("Datos en preorden: ");
                        PreordenRecursivo();
                        Console.WriteLine();
                        break;
                    case 2:     //Recorrido en inorden
                        Console.Write("Datos en inorden: ");
                        InordenRecursivo();
                        Console.WriteLine();
                        break;
                    case 3:     //Recorrido en postorden
                        Console.Write("Datos en postorden: ");
                        PostordenRecursivo();
                        Console.WriteLine();
                        break;
                    case 4:     //Recorrido en postorden
                        Console.Write("Datos por anchura: ");
                        Anchura();
                        Console.WriteLine();
                        break;
                }
            }
            else Console.WriteLine("Árbol Vacío :(");
        }     
        private void PreordenRecursivo()
        {
                Console.Write(contenido + " ");
                if (izq != null)
                    izq.PreordenRecursivo();
                if (der != null)
                    der.PreordenRecursivo();
        }   
        private void InordenRecursivo()
        {
            if (izq != null)
                izq.InordenRecursivo();
            Console.Write(contenido + " ");
            if (der != null)
                der.InordenRecursivo();
        }
        private void PostordenRecursivo()
        {
            if (izq != null)
                izq.PostordenRecursivo();
            if (der != null)
                der.PostordenRecursivo();
            Console.Write(contenido + " ");
        }
        private void Anchura()
        {
            Queue<Nodo> cola = new Queue<Nodo>();
            cola.Enqueue(this);
            Nodo p;
            while (cola.Count != 0)
            {
                p = cola.Dequeue();
                Console.Write($"{p.contenido} ");
                if (p.izq != null)
                    cola.Enqueue(p.izq);
                if (p.der != null)
                    cola.Enqueue(p.der);
            }
        }
        public int Contar()         //Devuelve la cantidad de nodos de un árbol
        {
            int respuesta = 0;
            if (Vacio())
                Console.WriteLine("Árbol Vacío, sin nodos");
            else
            {
                CountRec(ref respuesta);
                Console.WriteLine($"El árbol tiene {respuesta} nodos");
            }
            return respuesta;
                    }
        private void CountRec(ref int contador)
        {
            if (izq != null )
                izq.CountRec(ref contador);
            contador++;
            if (der != null)
                der.CountRec(ref contador);
        }
        public int ContarHojas()         //Devuelve la cantidad de nodos de un árbol
        {
            int respuesta = 0;
            if (Vacio())
                Console.WriteLine("Árbol Vacío, sin hojas");
            else
            {
                CountRecSheets(ref respuesta);
                Console.WriteLine($"El árbol tiene {respuesta} hojas");
            }
            return respuesta;
        }
        private void CountRecSheets(ref int contador)
        {
            if (izq != null)
                izq.CountRecSheets(ref contador);
            if (this.izq == null && this.der == null)
                contador++;
            if (der != null)
                der.CountRecSheets(ref contador);
        }
        public void Generar()       //Permite al usuario final ingresar un árbol desde la consola
        {
            Eliminar();
            Console.Write("¿Cuál es la raíz: ");
            contenido = Console.ReadLine();
            GenerarRec();
            Console.WriteLine("Árbol correctamente generado");
        }
        private void GenerarRec()       //Parte recursiva del anterior para todos los demás nodos menos de la raíz
        {
            string dato, op;
            //Se opera por la izquierda
            Console.Write($"Nodo {this.contenido}. Tiene hijo por la izquierda (s/n)?: ");
            op = Console.ReadLine().ToLower();
            if (op == "s")
            {
                Console.Write("Valor: ");
                dato = Console.ReadLine();
                this.izq = new Nodo(dato);
                this.izq.GenerarRec();
            }
            //Se opera por la derecha
            Console.Write($"Nodo {this.contenido}. Tiene hijo por la derecha (s/n)?: ");
            op = Console.ReadLine().ToLower();
            if (op == "s")
            {
                Console.Write("Valor: ");
                dato = Console.ReadLine();
                this.der = new Nodo(dato);
                this.der.GenerarRec();
            }
        }
        public void Eliminar()      //Deja el árbol vacío
        {
            contenido = null;
            izq = der = null;
        }
        public bool EliminarSubArbolIzq(string buscado)     //Elimina el subarbol izquierdo al nodo que se reciba en el argumento 
        {
            bool respuesta = false;
            Nodo p = null;      //Apuntará al nodo que se encuentre para eliminar el subarbol izquierdp a ese
            p = BuscarInOrden(buscado);
            if (p != null)
                if (p.izq != null)
                {
                    p.izq = null;
                    respuesta = true;
                }        
            return respuesta;
        }
        public bool EliminarSubArbolDer(string buscado)     //Elimina el subarbol izquierdo al nodo que se reciba en el argumento 
        {
            bool respuesta = false;
            Nodo p = null;      //Apuntará al nodo que se encuentre para eliminar el subarbol izquierdp a ese
            p = BuscarInOrden(buscado);
            if (p != null)
                if (p.der != null)
                {
                    p.der = null;
                    respuesta = true;
                }
            return respuesta;
        }
        public Nodo SubarbolEliminadoIzquierdo(string buscado)     //Muestra por pantalla el subarbol eliminado y 
        {
            Nodo respuesta = null;
            Nodo p;
            p = BuscarInOrden(buscado);
            if (p != null)
            {
                if (p.izq != null)
                {
                    respuesta = p.izq;
                    p.izq = null;
                }
            }
            return respuesta;       //Si quiero que imprima el nodo solo pongo respuesta.recorrido
        }
        public Nodo SubarbolEliminadoDerecho(string buscado)     //Muestra por pantalla el subarbol eliminado y 
        {
            Nodo respuesta = null;
            Nodo p;
            p = BuscarInOrden(buscado);     //Nodo p = Sub.......(buscado) --}> p.recorrido(2);
            if (p != null)
                if (p.der != null)
                {
                    respuesta = p.der;
                    p.der = null;
                }
            return respuesta;   //Si quiero que imprima el nodo solo pongo respuesta.recorrido
        }
        public Nodo BuscarInOrden(string buscado)       //Busca a buscado y devuelve un apuntador a ese nodo si existe, null si el método no existe
        {
            Nodo r = null;
            int it = 1;  //Numero de iteracion de las llamadas recursivas
            if (!Vacio())
                BuscarInOrdenRec( buscado, ref r, ref it);
            return r;   //Si quiero que imprima el nodo solo pongo r.recorrido
        }
        private void BuscarInOrdenRec(string buscado, ref Nodo r, ref int it)
        {
            if (izq != null)
                izq.BuscarInOrdenRec(buscado, ref r, ref it);
            if (buscado == this.contenido)
            {
                if (it == 1)
                    r = this;
                it++;
            }
            if (der != null)
                der.BuscarInOrdenRec(buscado, ref r, ref it);
        }
        public int EliminarSubArbolIzqIteracionesTotales(string buscado)     //Elimina el subarbol izquierdo al nodo que se reciba en el argumento y todas sus iteraciones
        {
            int respuesta =0;
            Nodo[] p;      //Apuntará al nodo que se encuentre para eliminar el subarbol izquierdp a ese
            p = BusInOrden(buscado);
            int i = 0;
            while (p[i] != null)    //Para que recorra unicamente por los Nodos que tienen valor en el arreglo
            {
                if (p[i].izq != null)       // Verifica si el nodo izquierdo no está vacío para eliminarlo 
                {
                    p[i].izq = null;
                    respuesta++;
                }
                i++;
            }
            return respuesta;
        }
        public Nodo[] BusInOrden(string buscado)       //Busca a buscado y devuelve un apuntador a ese nodo si existe, null si el método no existe
        {
            Nodo[] r = new Nodo[15];
            int nodos =0;
            if (!Vacio())
                BuscaInOrdenRec(buscado, ref r, ref nodos);
            return r;   //Si quiero que imprima el nodo solo pongo r.recorrido
        }
        private void BuscaInOrdenRec(string buscado, ref Nodo[] r, ref int nodos)
        {
            if (izq != null)
                izq.BuscaInOrdenRec(buscado, ref r, ref nodos);
            if (buscado == this.contenido)
            {
                r[nodos] = this;
                nodos++;
            }
            if (der != null)
                der.BuscaInOrdenRec(buscado, ref r, ref nodos);
        }
        public bool InsertarIzquierda(string buscado, string dato, bool todos, bool izq)
        {
            bool respuesta = false;
            Nodo nuevo;
            Nodo aux;
            Nodo temp;
            if (todos)
            {
                //Falta programar
                if (BuscarInOrden(buscado) != null)
                {
                    while (BuscarInOrden(buscado) != null)       //Mientras buscado exista tendra que hacer lo que se le pide
                    {
                        nuevo = new Nodo(dato);
                        aux = BuscarInOrden(buscado);
                        if (aux.izq != null)        //Si existe un subárbol izquierdo del buscado entonce tiene que apuntar a nuevo y guardarse el subarbol
                        {
                            temp = aux.izq;        //Se guarda temporalmente el subarbol izquierdo del buscado para colocarlo a la izquierda de nuevo
                            aux.izq = nuevo;
                            if (izq)
                                nuevo.izq = temp;
                            else nuevo.der = temp;
                        }
                        else aux.izq = nuevo;
                        aux.contenido = "*CAMBIARVAL*";
                    }
                    while (BuscarInOrden("*CAMBIARVAL*") != null)
                    {
                        aux = BuscarInOrden("*CAMBIARVAL*");
                        aux.contenido = buscado;
                    }
                    respuesta = true;
                }  
            }
            else
            {
                if (BuscarInOrden(buscado) != null)
                {
                    nuevo = new Nodo(dato);
                    aux = BuscarInOrden(buscado);
                    if (aux.izq != null)        //Si existe un subárbol izquierdo del buscado entonce tiene que apuntar a nuevo y guardarse el subarbol
                    {
                        temp = aux.izq;        //Se guarda temporalmente el subarbol izquierdo del buscado para colocarlo a la izquierda de nuevo
                        aux.izq = nuevo;
                        if (izq)
                            nuevo.izq = temp;
                        else nuevo.der = temp;
                    }
                    else aux.izq = nuevo;
                    respuesta = true;
                }
            }
            return respuesta;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Nodo Arbol = new Nodo();
            Arbol.DatosPrueba();        //Se colocan los datos de prueba
            Arbol.EliminarSubArbolIzqIteracionesTotales("B");
            Arbol.Recorrido(2);
            bool respp = Arbol.InsertarIzquierda("B", "Z", true, true);
            Arbol.Recorrido(2);
            Console.ReadKey();        
        }
    }
}